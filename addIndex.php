<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style2.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
    <link rel="stylesheet" href="https://maxCDn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Add Page</title>
</head>

<body> 
<h1 >Add Items!</h1>
<a  href="Index.php" >Home Page</a>
    <select  name="cases" id ="cases">
        <option>Type Switcher</option>
        <option value="chairs">Chairs</option>
        <option value="books">Books</option>
        <option value="cds">CD's</option>
    </select>

<div  class="chairs" id="chairs">
    <form action="add.php" method="POST" id="chair" >
        <input type="hidden" name="sku" value="CH">
        <input type="text" name="id" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="SKU" required>
        <input type="text" name="name" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Name" required>
        <input type="number" name="price" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Price" required>
        <input type="text" name="special" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Dimensions" required>
        <!-- neatly displays form errors -->
        <div id="card-errors" role="alert"></div>
        <button>Submit</button>
    </form>
</div>

<div  class="books" id="books">
    <form action="add.php" method="POST" id="book">
        <input type="hidden" name="sku" value="BK">
        <input type="text" name="id" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="SKU" required>
        <input type="text" name="name" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Name" required>
        <input type="number" name="price" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Price" required>
        <input type="number" name="special" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Weight" required>       
        <!-- neatly displays form errors -->
        <div id="card-errors" role="alert"></div>
        <button>Submit</button>
    </form>
</div> 

<div  class="cds" id="cds">
    <form action="add.php" method="POST" id="cd">
        <input type="hidden" name="sku" value="CD">
        <input type="text" name="id" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="SKU" required>
        <input type="text" name="name" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Name" required>
        <input type="number" name="price" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Price" required>
        <input type="number" name="special" class="form-add mb-3 StripeElement StripeElement--empty" placeholder="Size" required>
        <!-- neatly displays form errors -->
        <div id="card-errors" role="alert"></div>
        <button>Submit</button>
    </form>
</div>  
<!-- jQuery to hide divs that are not selected -->
<script> 
$('div').hide()
    $('#cases').change(function () {
        var value = this.value;
    $('div').hide()
    $('#' + this.value).show();
});
</script>
</body>
</html>
<?php

class Database
{
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    private $limit = PER_PAGE;

    private $dbh;
    private $error;
    private $stmt;

    public function __construct()
    {
        //construct automatically connects to db
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $options = array(PDO::ATTR_PERSISTENT=>true, PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
        
        try
        {
            $this->dbh = new PDO($dsn, $this->user, $this->pass,  $options);
        }
        catch(PDOException $e)
        {
            $this->error = $e->getMessage();
        }
    }

    function __destruct() {
        //  closes connection to db
      
          if ($this->stmt !== null) { $this->stmt = null; }
          if ($this->dbh !== null) { $this->dbh = null; }
        }

    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if(is_null($type))
        {
            switch(true)
            {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    function countAll()
    {
        //  number of users, to know how many items to display per page
        $number = 0;
        $this->stmt = $this->dbh->prepare("SELECT COUNT(*) count FROM items");
        $this->stmt->execute();
        while ($row = $this->stmt->fetch(PDO::FETCH_NAMED))
        {
            $total = $row['count']; 
        }
        return $total;
    }
    
    function getAll($limit=null)
    {
    // getAll() : get all users
    // PARAM $limit : limits number of entries
        $users = [];
        $sql = "SELECT * FROM items";
        if ($limit!=null) 
        { 
            $sql .= $limit; 
        }
        $this->stmt = $this->dbh->prepare($sql);
        $this->stmt->execute();
        while ($row = $this->stmt->fetch()) 
        { 
            $users[] = $row; 
        }
        return count($users)==0 ? false : $users ;
    }
   
}
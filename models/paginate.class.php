<?php

include_once 'users/pdo_db.php';
include_once 'includes/autoloader.php';

class Paginate
{

    private $pgtotal; // total number of entries
    private $pgnow; // current page
    private $perpage; // entries per page
    private $pgall; // total number of pages
    private $pgX; // starting entry number

    function pagination($total, $pgnow, $perpage)
    {
        // pagination calculations
        // PARAM $total : total number of entries
        // $pgnow : current page number
        // $perpage : number of entries per page

        if (!is_numeric($total)) { $total = 0; } //if total number of entries not set make it 0
        if (!is_numeric($pgnow)) { $pgnow = 1; } //if current page not set make it 1
        if (!is_numeric($perpage)) { $perpage = 10; } // if PARAM PER_PAGE set to 0 overrides it else leaves it as it is 
        $this->pgtotal = $total;
        $this->pgnow = $pgnow;
        $this->perpage = $perpage<=0 ? 10 : $perpage; // checks if PER_PAGE is more than 0
        $this->pgall = CEIL($this->pgtotal / $this->perpage); // if the division is not a whole number rounds it up, so we dont lose pages if division is with reminder
        $this->pgX = $this->perpage * ($this->pgnow-1);
    }

    function limit() 
    {
        // limits number of items that can be queried in a single page from MySQL
        return " LIMIT ".$this->pgX.",".$this->perpage;
    }

    private $pgurl; // URL for anchor link

    function draw($url="")
    {
        // draw() : draw HTML pagnatiion numbers 
        // PARAM $url : URL or JS for anchor link
        $this->pgurl = $url;

        echo '<nav class="pagination">';      
        for ($i=1; $i<=$this->pgall; $i++) { $this->pageNumber($i); }   //printing page numbers
        echo '</nav>';
    }

    function pageNumber($pg)
    {
        printf (
            '%s->%s%u', //manually adding arrow and using placeholders for arguments
            $pg==$this->pgnow ? " " : " ", //do not print current page on top of all other pages
            "<a href='$this->pgurl$pg'>", // href to go to selected page with $_GET from showPagination()
            $pg,
            "</a>"
        );
    }

}
<?php
include_once 'users/pdo_db.php';
include_once 'includes/autoloader.php';

class Delete
{
    private $delete;

    public function __construct()
    {            
        $this->delete = new Database();
    }

    public function deleteItem()
    {          
        if (isset($_POST['delete']))
        {
            if(isset($_POST['check']))
            {
                foreach($_POST['check'] as $selected)
                {                   
                    $this->delete->query("DELETE FROM items WHERE id = '$selected'"); 
                    //execute it
                    $this->delete->execute();                   
                }
                echo '<script type="text/javascript">alert("Succeasfully Deleted!")</script>';             
            }                                         
        }  
    } 

}
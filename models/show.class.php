<?php

include_once 'users/pdo_db.php';
include_once 'includes/autoloader.php';

class Show
{
    
    private $userLib;
    private $pageLib;
    private $delete;

    public function __construct()
    {     
        $this->userLib = new Database();
        $this->pageLib = new Paginate();
        $this->delete = new Database();
    }
    
    public function showItems()
    {        
        $page = isset($_GET['page']) ? $_GET['page'] : 1; // current page
        
        $this->pageLib->pagination($this->userLib->countAll(), $page, PER_PAGE);
        // get data from db
        $items = $this->userLib->getAll($this->pageLib->limit());

        if($items!=null)
        {
            foreach ($items as $item) 
            {
                echo '<div class="items"><input class="check" type="checkbox" name="check[]" value='.$item['id'].'>'.$item['id']."<br>".$item['name']."<br>".$item['price']."$"."<br>".$item['special'].'</div>';
            }  
        }
        else
        {
            echo "Databse is empty! Use the dummy data to populate it.";
        }
    }

    public function showPagination()
    {
        //get requested page number, or start at first page by default
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $this->pageLib->pagination($this->userLib->countAll(), $page, PER_PAGE);
        $this->pageLib->draw("?page=");
    }
}

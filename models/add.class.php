<?php

class Add
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function addItem($data)
    {
        //Prepareing query to be binded
        $this->db->query('INSERT INTO items(id, name, price, special) VALUES (:id, :name, :price, :special)');

        //Start binding
        $this->db->bind(':id', $data['sku'].'_'.$data['id'] );
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':special', $data['special']);

        //execute it
        if($this->db->execute())
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}   
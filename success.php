<?php
    if(!empty($_GET['name'] && !empty($_GET['price'])))
    {
        $GET =  filter_var_array($_GET, FILTER_SANITIZE_STRING);

        $name = $GET['name'];
        $price = $GET['price'];
    }
    else
    {
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Thank You!</title>
</head>
<body>
    <div class="container mt-4">
        <h2>You have succesfully added an item to databse with the name of: <?php echo $name; ?>.</h2>      
        <h2>Price of siad item is:  <?php  echo $price; ?>.</h2>
        <hr>
        <p><a href="index.php" class="btn btn-light mt-2">Go Back</a></p>
    </div>
</body>
</html>
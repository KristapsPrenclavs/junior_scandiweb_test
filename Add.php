<?php
require_once('config/sampleAdmin.php');
require_once('users/pdo_db.php');
include 'includes/autoloader.php';

$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

$id = $POST['id'];
$name = $POST['name'];
$price = $POST['price'];
$special = $POST['special'];
$sku = $POST['sku'];


$itemData = [
    'id' => $id,
    'name' => $name,
    'price' => $price,
    'special' => $special,
    'sku' => $sku    
];


$item = new Add();
$item->addItem($itemData);

header('Location: success.php?name='.$name.'&price='.$price);